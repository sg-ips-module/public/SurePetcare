# SurePetcare

[![Version](https://img.shields.io/badge/Symcon-PHPModul-red.svg)](https://www.symcon.de/)
[![Version](https://img.shields.io/badge/Modul%20Version-1.2%20Build:231118-blue.svg)]()
[![Version](https://img.shields.io/badge/Symcon%20Version->=%206.0-green.svg)]()
[![Version](https://img.shields.io/badge/Spenden-PayPal-yellow.svg?style=flat-square)](https://www.paypal.com/paypalme2/Simon6714?locale.x=de_DE)

---

Symcon Modul für Sure Petcare Connect (Haustier-, Hund- und Katzenklappen)

**Hinweis:**  
- Die Sure Petcare API ist nicht öffentlich verfügbar, dies heißt das bei Änderung der API dieses Modul wahrscheinlich nicht mehr funktionieren wird

# Generell
Ich habe bei der Erstellung des Moduls großen Wert auf Effizienz bei der Abholung der Cloud-Daten gelegt.  
- Die Hauptdaten sind in **einem** CURL Aufruf enthalten (siehe SPET_ReadArray)
- Die Optionalen Daten der Timeline benötigen einen zweiten CURL Aufruf

---
# Inhaltsverzeichnis

- [SurePetcare](#surepetcare)
- [Generell](#generell)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Funktionsumfang](#funktionsumfang)
- [Systemanforderungen](#systemanforderungen)
- [Installation](#installation)
- [Wichtige Hinweise](#wichtige-hinweise)
- [Befehlsreferenz](#befehlsreferenz)
  - [A. Einfache Benachrichtigungen per Aktion-Script](#a-einfache-benachrichtigungen-per-aktion-script)
    - [A1. Eintrag ins Logfile, wenn Katzenklappe passiert wurde](#a1-eintrag-ins-logfile-wenn-katzenklappe-passiert-wurde)
    - [A2. Benachrichtigung per Alexa](#a2-benachrichtigung-per-alexa)
  - [B. Benachrichtigung per Daten-Array](#b-benachrichtigung-per-daten-array)
    - [B1. Benachrichtigung bei leeren Batterien](#b1-benachrichtigung-bei-leeren-batterien)
    - [B2. Daten-Array von Modul in Scripts nutzen](#b2-daten-array-von-modul-in-scripts-nutzen)
  - [## C. Benachrichtigung per Chronik/Timeline-Array](#-c-benachrichtigung-per-chroniktimeline-array)
- [Autoren](#autoren)
- [Lizenz](#lizenz)
- [Screenshots](#screenshots)
- [FAQ](#faq)

---
# Funktionsumfang

- Sure Petcare Connect nutzt die surehub.io API
- Hub: Anzeige und ändern der LEDs
- Haustier- und Katzenklappe: Statusanzeige und einstellen des Sperrmodus (Aus, Eingehend, Ausgehend, Beidseitig)
- Haustier: Statusanzeige und ändern des Aufenthalt-Ortes ("Im Haus" oder "Draußen")
- Arrays mit Informationen von der Cloud-API, siehe Befehlsreferenz
- Variable für Batteriestatus in Prozent  (einfache Benachrichtigung per Aktion-Script)
- Variable für Verbindungsstatus
- Variable für Haustier-Klappe (einfache Benachrichtigung per Aktion-Script)
- Variable für letzten Haustier Aufenthaltsort Wechsels
- Variable für Felaqua Füllstand
- Variable für Felaqua zuletzt aufgefüllt am
- Weitere Benachrichtigungen können per Benachrichtigung- und Timeline-Array erfolgen

---
# Systemanforderungen

- IP-Symcon ab Version 6.0 (keine Legacy-Console Unterstützung!)
- SurePetcare Connect Haustier- und Katzenklappen mit HUB
- SurePetcare Account


# Installation
Das Modul in Symcon einbinden:

1. In der IP-Symcon Console unter "Kern Instanzen" die "Module Control" Instanz öffnen
2. Auf "Hinzufügen" klicken, hier ist die GIT-URL einzutragen:  
   <https://gitlab.com/SG-IPS-Module/Public/SurePetcare.git>
3. Nun kann im IPS-Objektbaum (an beliebiger Stelle) eine neue Modul-Instanz hinzufügt werden

---
# Wichtige Hinweise
 - Bei Nutzung des testing Branch, bei Aktualisierung des Moduls bitte alle Variablen des Moduls löschen!
   - Alle Variablen des Moduls werden bei Update (manuell oder durch Timer) neu erstellt

---
# Befehlsreferenz
## A. Einfache Benachrichtigungen per Aktion-Script
### A1. Eintrag ins Logfile, wenn Katzenklappe passiert wurde
In diesem Beispiel möchten wir erreichen, dass ein Eintrag in das Symcon Logfile erfolgt, wenn eine Katze durch die Klappe geht.  
1. Erstellen des Skripts.
2. Hinzufügen von Ereignis bei Variablenänderung auf die Variable "Letzte Bewegung".
3. Das Script mit der rechten Maustaste auf *→ PHP-Skript bearbeiten → Visuelle Einstellungen → Objekt anzeigen*  
  und deaktivieren die Anzeige, so das dieses im WebFront nicht angezeigt wird.  
  
![Screenshot WebFront](imgs/Script.png)

Inhalt des Skriptes:
```
<?php
//Eintrag in das Logfile wenn Maxi die Katzenklappe passiert
//Wähle als Ereignis Auslöser hier die Modul Variablen "Letzte Bewegung" (IDENT: PetMotion_xxxxxx)

$Petname_Log = IPS_GetName($_IPS['VARIABLE']);              // Katzennamen aus Variablenamen lesen
$FlapAndTime = GetValueString($_IPS['VARIABLE']);           // Text aus Variablen Wert lesen
$Nachricht = $Petname_Log." ist durch ".$FlapAndTime;       // Text für Logfile zusammensetzen

IPS_LogMessage($_IPS['SELF'], $Nachricht);                  // Nachricht in das IPS Logfile schreiben
```
---
**TIP:** Bei mehreren Haustieren kann das selbe Script benutzt werden. Einfach ein weiteres Ereignis auf weitere "Letzte Bewegung" Variable hinzufügen

---

Die Ausgabe im Logfile sieht wie folgt aus.
>05.01.2022 17:49:28 | 00000 | CUSTOM  | 19275                | Maxi's letzte Bewegung ist durch Katzenklappe am 05.01 um 17:49h nach Außen  
>05.01.2022 17:42:51 | 00000 | CUSTOM  | 19275                | Maxi's letzte Bewegung ist durch Katzenklappe am 05.01 um 17:42h ins Haus  

### A2. Benachrichtigung per Alexa
Inhalt des Skriptes:
```
<?php
//Nachricht per Alexa wenn Katze durch Katzenklappe geht
//Wähle als Ereignis Auslöser hier die Modul Variablen Katzenname (IDENT: PetLocation_xxxxxx)

$Petname_Alexa = IPS_GetName($_IPS['VARIABLE']);            // Katzennamen aus Variablenamen lesen
$Location = GetValueInteger($_IPS['VARIABLE']);             // 1=inside, 2=outside
if ($Location == 1) {
    $Text = " ist soeben nach Hause gekommen!";
} else {
    $Text = " ist soeben raus gegangen.";
}
$Ansagetext = "Miau, miau. ".$Petname_Alexa.$Text;

... usw ...
```
---
## B. Benachrichtigung per Daten-Array
### B1. Benachrichtigung bei leeren Batterien
Diese Variante ist unabhängig von der Einstellung "Zusätzliche Variablen für Batterien" da die Werte direkt aus dem Daten-Array des Moduls gelesen werden.
```
<?php
//Script zur Benachrichtigung bei leeren Sure Petcare Batterien..

$ARRAY = SPET_ReadDevices(26066);                                    // Batteriestatus per Array abfragen

if (is_array($ARRAY)) {                                              // Sicherstellen das Array nicht leer ist (falls noch kein Update nach Neustart..)
    foreach ($ARRAY as $Devices) {                                   // Alle Devices durchlaufen
        $battery=@$Devices['status']['battery'];                     // Alle Devices die einen battery Wert haben
        if ($battery) {                                              // Wenn Batteriewert vorhanden Benachrichtigungsfunktion aufrufen
            Benachrichtigung($Devices['name'], $battery);
        }
        
    }
}

function Benachrichtigung($Name, $Volt) {
    #echo $Name.": ".$Volt."\n";
    if ($Volt < 5.05) {
        echo "Bitte Batterien wechseln bei Sure Petcare ".$Name;     // Anstatt "echo" hier nun deine gewünschte Benachrichtigung einbauen..
    }
}
```

---
### B2. Daten-Array von Modul in Scripts nutzen
Die Rohdaten der Sureflap Cloud können über das Modul abgerufen, und in Scripts weiter verarbeitet werden.  
Folgende Daten Arrays stehen zur Verfügung: (12345=Instanz des Moduls!)  
```
SPET_ReadHousehold(12345);
SPET_ReadDevices(12345);
SPET_ReadPets(12345);
SPET_ReadArray(12345);
```
In einem Symcon Script könnte das so aussehen:
```
<?php
//Lese Geburtstage der Katzen..
$ARRAY = SPET_ReadPets(12345);
foreach ($ARRAY as $CATS) {
    echo $CATS['name']." hat am ".date("d.m", strtotime($CATS['date_of_birth']))." Geburtstag\n" ;
}
```
Die Ausgabe sieht bei mir so aus:
```
Maxi hat am 23.07 Geburtstag
Milla hat am 05.08 Geburtstag
```
---
## C. Benachrichtigung per Chronik/Timeline-Array
---
**Hinweis:**  
Diese Daten werden von der *surehub.io* API mit grosser Zeitverzögerung ausgeliefert!  
Somit leider nicht für eine zeitnahe Benachrichtigung geeignet :-(

---
**Voraussetzungen:**  
1. Im Modul muss **Chronik (HTML)** aktiviert werden
2. Als **Ereignis Auslöser** (für das Skript) muss die **Chronik Variable** definiert werden

Skript Beispiel:
```
<?php
$SurePetcare = 52724;                                                                      # Instanz des SurePetcare Modules
$ARRAY = SPET_ReadTimeline($SurePetcare);                                                  # Welches Array

if (is_array($ARRAY)) {                                                                    # Sicherstellen das Array nicht leer ist
    if ($ARRAY[0]['TYPE'] === 30) {                                                        # TYPE siehe Dokumentation
        WFC_SendNotification(18102, $ARRAY[0]['TIME'], $ARRAY[0]['TEXT'], "Cat", 3600);    # Meldung für 1 Stunde im Webfront anzeigen
    }
}
```
  
**Bekannte Event Typen im Array:**  

Im Modul sind noch nicht alle dieser bekannten Event types hinterlegt, da ich diese Geräte nicht selbst besitze, Hilfe ist Willkommen.

|Type|Bedeutung|
|--------|--------|
|0|   Haustier geht durch eine Tür|
|1|   Batterieschwelle erreicht|
|7|   Unbekannte Bewegung der Tür|
|21|  Schale aufgefüllt|
|22|  Haustier hat gegessen|
|24|  Klappe wurde zurückgesetzt|
|29|  Haustier hat getrunken|
|30|  Felaqua aufgefüllt|
|32|  Erinnerung Frischwasser für Felaqua|
|34|  Unbekannt hat getrunken|

---
# Autoren

- [@Simon6714](https://github.com/Simon6714)

---
# Lizenz

Dieses Projekt unterliegt der [CC BY-NC-SA 4.0 Lizenz](https://creativecommons.org/licenses/by-nc-sa/4.0/) , und ist für die nicht kommerzielle Nutzung kostenlos.  
Schenkungen zur Unterstützung werden hier angenommen.  

[![](https://www.paypalobjects.com/de_DE/DE/i/btn/btn_donate_LG.gif)](https://www.paypal.me/Simon6714?locale.x=de_DE)

---
# Screenshots

WebFront:  
![Screenshot WebFront](imgs/WebFront.png)

Konfigurationsformular:  
![Screenshot Formular](imgs/Formular.png)

---
# FAQ
- Ich habe ein Haustier aus der Sure Petcare App gelöscht, diese wird aber weiterhin im Modul angezeigt.  
  - Einfach die Variable manuell löschen
  - Alle Variablen des Moduls werden bei Update (manuell oder durch Timer) neu erstellt  
- Ich möchte eine Benachrichtigung, falls ein Tier die Klappe benutzt.  
  - Siehe Befehlsreferenz  
- Wie sieht es mit Unterstützung für die Ausgangssperre aus?
  - Die Möglichkeiten von Symcon um Ausgangssperren zu definieren sind vielfältiger so das die App dafür nicht benötigt wird
    - Einfach: Wochenplan in Symcon
    - Experte: Erweitertes Script mit Beachtung von Anwesenheit, Uhrzeit, Helligkeitssensor, usw.
- Wird der Futter- oder Wasserspender unterstützt?
    - Leider nein, da ich keinen besitze (und somit nicht integrieren kann)
- Batterie Variable, ab welchem Volt Wert sollten die Batterien gewechselt werden?
    - Bei der Haustierklappe Connect gilt kleiner/gleich 5.05 Volt als leer