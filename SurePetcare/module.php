<?php
//© by Simon Grass(SimonS)

require_once __DIR__ . '/../libs/curl.php';

// Klassendefinition
class SurePetcare extends IPSModule
{
    use SPET_CURL;

    // Überschreibt die interne IPS_Create($id) Funktion
    public function Create()
    {

        // Diese Zeile nicht löschen.
        parent::Create();

        //Attribute registrieren
        $this->RegisterAttributeString('AuthToken', '');
        $this->RegisterAttributeString('BaseURL', 'https://app.api.surehub.io');
        $this->RegisterAttributeInteger('HouseholdID', 0);

        //Variablen registrieren
        $this->RegisterPropertyString('Username', '');
        $this->RegisterPropertyString('Password', '');
        $this->RegisterPropertyInteger('UpdateIntervall', 20);
        $this->RegisterPropertyInteger('LineNumbers', 15);
        $this->RegisterPropertyBoolean('UpdateTimer', false);
        $this->RegisterPropertyBoolean('PetDetectionVariable', false);
        $this->RegisterPropertyBoolean('PetTestChip', false);
        $this->RegisterPropertyBoolean('Timeline', false);
        $this->RegisterPropertyBoolean('Notifications', false);
        $this->RegisterPropertyBoolean('MobileFont', false);
        $this->RegisterPropertyBoolean('OptionalColumns', false);
        $this->RegisterPropertyBoolean('Debug', false);

        //HTML
        $this->RegisterPropertyInteger('HTMLBG', -1);
        $this->RegisterPropertyInteger('HTMLFontType', 0);
        $this->RegisterPropertyInteger('HTMLFontColor', 14474460);
        $this->RegisterPropertyInteger('HTMLFontSize', 16);
        $this->RegisterPropertyInteger('HTMLPaddingHigh', 0);
        $this->RegisterPropertyInteger('HTMLPadding1', 60);
        $this->RegisterPropertyInteger('HTMLPadding2', 140);

        //Profile erstellen
        $this->CreateProfiles();

        //Variablen erstellen
        //werden über die Update Funktion erstellt..

        //Timer registrieren
        $this->RegisterTimer('SPET_Update', 0, 'SPET_Update($_IPS[\'TARGET\']);');
    }

    // Überschreibt die intere IPS_ApplyChanges($id) Funktion
    public function ApplyChanges()
    {
        // Diese Zeile nicht löschen
        parent::ApplyChanges();

        //Register Kernel Messages
        $this->RegisterMessage(0, IPS_KERNELSTARTED);

        //IP-Symcon Kernel bereit?
        if (IPS_GetKernelRunlevel() !== KR_READY) {
            $this->SendDebug(__FUNCTION__, $this->Translate('Kernel is not ready! Kernel Runlevel = ') . IPS_GetKernelRunlevel(), 0);
            return false;
        }

        //Check Credentials
        $Result = $this->CheckCredentials();
        if ($Result > 102) {
            $this->SetStatus($Result);
            return false;
        }

        //Timer-Intervall setzen
        $this->SetMyTimerInterval();

        //Check OK
        $this->SetStatus(102);

        //Apply
        if (empty($this->GetBuffer("DataArray"))) {
            $this->Update();
        } else {
            if ($this->ReadPropertyBoolean('Notifications') or $this->ReadPropertyBoolean('Timeline')) {
                $this->GetOptionalsData();
            }
            if ($this->ReadPropertyBoolean('Notifications') == false) {
                @IPS_DeleteVariable(IPS_GetObjectIDByIdent('NotificationsHTML', $this->InstanceID));
            }
            if ($this->ReadPropertyBoolean('Timeline') == false) {
                @IPS_DeleteVariable(IPS_GetObjectIDByIdent('TimelineHTML', $this->InstanceID));
            }
        }

        return true;
    }

    public function Update()
    {
        $this->SetMyDebug(__FUNCTION__ . '()', 'Update run..');
        if ($this->Check_Token() === false) {
            return false;
        }

        //Get data from buffer
        $curl = unserialize($this->GetBuffer("DataArray"));

        //Get household
        $this->SetMyDebug(__FUNCTION__ . '(Household)', 'Get Household..');
        $households = @$curl['result']['data']['households'];
        #print_r($households);
        if ($curl['http_code'] == "200" and isset($households[0]['id'])) {
            foreach ($households as $index => $household) {
                $this->SetMyDebug(__FUNCTION__ . '(Household)', ' > ' . $households[0]['name']);
                $Household_Name = $household['name'];
                $Household_ID = $household['id'];
                //Create IPS Variable
                $Household_Ident = 'Household_' . $Household_ID;
                if (!@$this->GetIDForIdent($Household_Ident)) {
                    $this->RegisterVariableString($Household_Ident, $this->Translate('Household'));
                    IPS_SetIcon($this->GetIDForIdent($Household_Ident), 'HouseRemote');
                }
                $this->SetChangedValue($Household_Ident, $Household_Name);
                //Set household
                $this->WriteAttributeInteger('HouseholdID', $Household_ID);
            }
        } else {
            $this->SetMyDebug(__FUNCTION__ . '(Household)', "..No household!");
            return false;
        }

        //Get devices
        $this->SetMyDebug(__FUNCTION__ . '(Devices)', 'Get Devices..');
        $devices = @$curl['result']['data']['devices'];
        #print_r($devices);
        if (@$devices[0]['id']) {
            foreach ($devices as $index => $device) {
                $count = (int) $index + 1;
                $countdevice = (int) $index + 50;
                switch ($device['product_id']) {
                    case 1:
                        $devicetype = 'Hub';
                        #Create hub device
                        $Hub_Ident = 'Hub_' . $device['id'];
                        if (!@$this->GetIDForIdent($Hub_Ident)) {
                            $this->RegisterVariableInteger($Hub_Ident, 'Hub' . $count . ' LEDs', 'SPET.LED', 2);
                            $this->EnableAction($Hub_Ident);
                        }
                        break;
                    case 2:
                        $devicetype = 'Repeater';
                        break;
                    case 3:
                        $devicetype = 'Pet Door Connect';
                        #If empty cloud name
                        $Flap_Name = @$device['name'];
                        if (empty($Flap_Name)) {
                            $Flap_Name = $this->Translate('Flap');
                        }
                        $Flap_Ident = 'Flap_' . $device['id'];
                        #Create flap device
                        if (!@$this->GetIDForIdent($Flap_Ident)) {
                            $this->RegisterVariableInteger($Flap_Ident, $Flap_Name . ' ' . $this->Translate('lock'), 'SPET.Lockmode', 3);
                            $this->EnableAction($Flap_Ident);
                        }
                        $this->Change_IpsVarName($Flap_Ident, $Flap_Name);
                        break;
                    case 4:
                        $devicetype = "Pet Feeder Connect";
                        #If empty cloud name
                        $Feed_Name = @$device['name'];
                        if (empty($Feed_Name)) {
                            $Feed_Name = $this->Translate('Feed dispenser');
                        }
                        $Feed_Ident = 'Feed_' . $device['id'];
                        break;
                    case 5:
                        $devicetype = "Programmer";
                        break;
                    case 6:
                        $devicetype = "DualScan Cat Flap Connect";
                        #If empty cloud name
                        $Flap_Name = @$device['name'];
                        if (empty($Flap_Name)) {
                            $Flap_Name = $this->Translate('Flap');
                        }
                        $Flap_Ident = 'Flap_' . $device['id'];
                        #Create flap device
                        if (!@$this->GetIDForIdent($Flap_Ident)) {
                            $this->RegisterVariableInteger($Flap_Ident, $Flap_Name . ' ' . $this->Translate('lock'), 'SPET.Lockmode', 3);
                            $this->EnableAction($Flap_Ident);
                        }
                        $this->Change_IpsVarName($Flap_Ident, $Flap_Name);
                        break;
                    case 8:
                        $devicetype = "Water Dispenser";
                        #If empty cloud name
                        $Water_Name = @$device['name'];
                        if (empty($Water_Name)) {
                            $Water_Name = $this->Translate('Water dispenser');
                        }
                        $Water_Ident = 'Water_' . $device['id'];
                        break;
                }
                $this->SetMyDebug(__FUNCTION__ . '(Devices)', ' > ' . $index . ' ' . $devicetype);
                unset($devicetype); //delete devicetyp, only for debug output

                //Optional in devices
                //Hub - led state
                if ($device['product_id'] === 1) {
                    $led_mode = @$device['control']['led_mode'];
                    #print_r($led_mode);
                    if (preg_match("/[014]/", $led_mode)) {
                        $this->SetMyDebug(__FUNCTION__ . '(Devices)', ' ... ledmode: ' . $led_mode);
                        if ($led_mode == 1) {
                            $led_mode = 2;
                        }
                        if ($led_mode == 4) {
                            $led_mode = 1;
                        }
                        if ($this->GetValue($Hub_Ident) != $led_mode) {
                            SetValueInteger($this->GetIDForIdent($Hub_Ident), $led_mode);
                        }
                    } else {
                        $this->SetMyDebug(__FUNCTION__ . '(Devices)', "No LED Mode!");
                    }
                }
                //Flap
                if ($device['product_id'] === 3 or $device['product_id'] === 6) {
                    //Lock mode
                    $lockmode = @$device['control']['locking'];
                    #print_r($lockmode);
                    if (preg_match("/[0-3]/", $lockmode)) {
                        $this->SetMyDebug(__FUNCTION__ . '(Devices)', ' ... lockmode ' . $lockmode);
                        if ($this->GetValue($Flap_Ident) != $lockmode) {
                            SetValueInteger($this->GetIDForIdent($Flap_Ident), $lockmode);
                        }
                    } else {
                        $this->SetMyDebug(__FUNCTION__ . '(Devices)', "No lock mode!");
                    }

                    // Curfew state
                    /* $this->SetMyDebug(__FUNCTION__ . '(Devices)', '...Curefew: Get time state..');
                    $curfew = $device['control']['curfew'];
                    it is better to use the symcon functions :-) */

                    //Battery state
                    $battery = floatval(@$device['status']['battery']);
                    $this->BatteryState($battery, $Flap_Ident, $Flap_Name);
                    //Connection state
                    $connection = floatval(@$device['status']['online']);
                    $this->ConnectionState($connection, $Flap_Ident, $Flap_Name);

                }
                //Feed Dispenser (battery)
                if ($device['product_id'] === 4) {
                    //Battery state
                    $battery = floatval(@$device['status']['battery']);
                    $this->BatteryState($battery, $Feed_Ident, $Feed_Name);
                    //Connection state
                    $connection = floatval(@$device['status']['online']);
                    $this->ConnectionState($connection, $Feed_Ident, $Feed_Name);
                }
                //Water Dispenser (battery)
                if ($device['product_id'] === 8) {
                    //Battery state
                    $battery = floatval(@$device['status']['battery']);
                    $this->BatteryState($battery, $Water_Ident, $Water_Name);
                    //Connection state
                    $connection = floatval(@$device['status']['online']);
                    $this->ConnectionState($connection, $Water_Ident, $Water_Name);
                }
            }
        } else {
            $this->SetMyDebug(__FUNCTION__ . '(Devices)', "No devices!");
            return false;
        }

        //Get pets
        $this->SetMyDebug(__FUNCTION__ . '(Pets)', 'Get Pets..');
        $pets = @$curl['result']['data']['pets'];
        #print_r($pets);
        if (@$pets[0]['id']) {
            #$pets = $result['data'];
            foreach ($pets as $index => $pet) {
                $this->SetMyDebug(__FUNCTION__ . '(Pets)', ' > ' . $index . ' ' . $pet['name']);
                #CreateVariable for PetLocation
                $Pet_Ident = 'PetLocation_' . $pet['id'];
                $Pet_TagID = $pet['tag_id'];
                if (!@$this->GetIDForIdent($Pet_Ident)) {
                    $this->RegisterVariableInteger($Pet_Ident, $pet['name'], 'SPET.PetLocation', 10);
                    $this->EnableAction($Pet_Ident);
                    $Pet_IpsName = $pet['name'];
                }

                //Get PetLocation
                $Pet_Location = @$pet['position']['where'];
                $this->SetMyDebug(__FUNCTION__ . '(Pets)', ' ... Location: ' . $Pet_Location);
                if (preg_match("/[1-2]/", $Pet_Location)) {
                    #print_r(petlocation);
                    $this->SetChangedValue($Pet_Ident, $Pet_Location);
                    if ($Pet_Location == "1") {
                        $Pet_LocationText = 'in to the house';
                    } elseif ($Pet_Location == "2") {
                        $Pet_LocationText = 'to outside';
                    }
                }
                //Get PetDetection (if enabled)
                $this->PetDetection($pet, $Pet_LocationText);
            }

            #Petcount
            if (!@$this->GetIDForIdent('Pets')) {
                $this->RegisterVariableInteger('Pets', $this->Translate('Pets'), '', 1);
                IPS_SetIcon($this->GetIDForIdent('Pets'), 'Calendar');
            }
            if ($this->ReadPropertyBoolean('PetTestChip') === true) {
                $this->SetChangedValue('Pets', $index);
            } else {
                $this->SetChangedValue('Pets', $index + 1);
            }
        } else {
            $this->SetMyDebug(__FUNCTION__ . '(Pets)', "...No pets!");
        }
        //Optionals
        $this->GetOptionalsData();
        //Create Variable LastUpdate
        if (!@$this->GetIDForIdent('LastUpdate')) {
            $this->RegisterVariableInteger('LastUpdate', $this->Translate('Last update'), '~UnixTimestamp', 1);
            IPS_SetIcon($this->GetIDForIdent('LastUpdate'), 'Clock');
        }
        //Set LastUpdate
        SetValueInteger($this->GetIDForIdent('LastUpdate'), time());
    }

    public function GetConfigurationForm()
    {
        $InfoArray = $this->ModulInfo();
        $Name = $InfoArray['Name'];
        $Version = $InfoArray['Version'];
        $Build = $InfoArray['Build'];
        return '{
            "elements":
                [
                    {"type": "Label", "caption": "' . $Name . ' - v' . $Version . ' (Build: ' . $Build . ') © by SimonS"},
                    {"type": "Label", "caption": "_________________________________________________________________________________________________________________________________________________________________________" },
                    {"type": "Label", "caption": "Connect accessdata (if changed, the token must be deleted)" },
                    {"type": "RowLayout", "items": [
                        {"type": "ValidationTextBox","name": "Username","caption": "Username", "width": "250px"},
                        {"type": "PasswordTextBox", "name": "Password", "caption": "Password", "width": "250px" }
                    ] },
                    {"type": "RowLayout", "items": [
                    {"type": "CheckBox", "name": "UpdateTimer", "caption": "Timer", "width": "250px" },
                    {"type": "NumberSpinner", "name": "UpdateIntervall", "caption": "Update Intervall in seconds (Minimum=10)", "width": "250px", "minimum": 10, "maximum": 3600}
                    ] },
                    {
                        "type": "ExpansionPanel",
                        "caption": "Optional variables",
                        "items": [
                            {"type": "RowLayout", "items": [
                                {"type": "CheckBox", "name": "Notifications", "caption": "Notifications (HTML)", "width": "250px" },
                                {"type": "CheckBox", "name": "Timeline", "caption": "Timeline (HTML)", "width": "250px" }
                            ] }
                        ]
                    },
                    {
                        "type": "ExpansionPanel",
                        "caption": "HTML settings",
                        "items": [
                            {"type": "RowLayout", "items": [
                                {"type": "Select", "name": "HTMLFontType", "caption": "Font", "width": "200px",
                                    "options": [
                                        { "caption": "Default", "value": 0 },
                                        { "caption": "Consolas", "value": 1 },
                                        { "caption": "Courier", "value": 2 },
                                        { "caption": "DejaVu Sans Mono", "value": 3 }
                                    ]
                                },
                                {"type": "Label", "caption": "          " },
                                {"type": "SelectColor", "name": "HTMLFontColor", "caption": "font color", "width": "200px" },
                                {"type": "Label", "caption": "          " },
                                {"type": "SelectColor", "name": "HTMLBG", "caption": "background color", "width": "200px" }
                            ] },
                            {"type": "RowLayout", "items": [
                                {"type": "NumberSpinner", "name": "HTMLFontSize", "caption": "font size", "width": "200px" },
                                {"type": "Label", "caption": "          " },
                                {"type": "NumberSpinner", "name": "LineNumbers", "caption": "Number of lines (0=unlimited)", "minimum": 0, "maximum": 100, "width": "200px" },
                                {"type": "Label", "caption": "          " },
                                {"type": "CheckBox", "name": "MobileFont", "caption": "Small font on mobile devices"}
                            ] },
                            {"type": "RowLayout", "items": [
                                {"type": "NumberSpinner", "name": "HTMLPaddingHigh", "caption": "Cell height", "width": "200px" },
                                {"type": "Label", "caption": "          " },
                                {"type": "NumberSpinner", "name": "HTMLPadding1", "caption": "Cell width (type)", "width": "200px" },
                                {"type": "Label", "caption": "          " },
                                {"type": "NumberSpinner", "name": "HTMLPadding2", "caption": "Cell width (others)", "width": "200px" }
                            ] },
                            {"type": "CheckBox", "name": "OptionalColumns", "caption": "Optional columns (household, device, pet)"}
                        ]
                    },
                    {"type": "CheckBox", "name": "PetTestChip", "caption": "Test chip in use (decreases the pets variable by 1)" },
                    {"type": "Label", "caption": "_________________________________________________________________________________________________________________________________________________________________________" },
                    {"type": "CheckBox", "name": "Debug", "caption": "Debug" }
                ],
                "actions":
                [
                    {"type": "RowLayout", "items": [
                        {"type": "Button", "caption": "Login test", "onClick": "SPET_Check_Login($id);" },
                        {"type": "Button", "caption": "Update", "onClick": "SPET_Update($id);" },
                        {"type": "Label", "caption": "                               " },
                        {"type": "Button", "caption": "CleanToken",  "confirm": "Delete authentication token?", "onClick": "print_r(SPET_CleanToken($id));" },
                        {"type": "Button", "caption": "CleanVar",  "confirm": "Delete all variables of the instance?", "onClick": "print_r(SPET_CleanUp($id));" }
                    ] },
                    {"type": "RowLayout", "items": [
                        {"type": "Button", "caption": "Household", "onClick": "print_r(SPET_ReadHousehold($id));" },
                        {"type": "Button", "caption": "Devices", "onClick": "print_r(SPET_ReadDevices($id));" },
                        {"type": "Button", "caption": "Pets", "onClick": "print_r(SPET_ReadPets($id));" },
                        {"type": "Button", "caption": "Notifications", "onClick": "print_r(SPET_ReadNotifications($id));" },
                        {"type": "Button", "caption": "Timeline", "onClick": "print_r(SPET_ReadTimeline($id));" }
                    ] },
                    {"type": "RowLayout", "items": [
                        {"type": "Button", "caption": "Website", "onClick": "echo \'https://ips.air5.net/\';" },
                        {"type": "Button", "caption": "Documentation", "onClick": "echo \'https://gitlab.com/sg-ips-module/public/SurePetcare/-/blob/master/README.md\';" }
                    ] }
                ],
                "status":
                [
                    {"code": 102, "icon": "active", "caption": "SurePetcare module is active" },
                    {"code": 201, "icon": "error", "caption": "ERROR: Access data are empty!" }
                ]
            }';
    }

    public function Check_Login()
    {
        $this->SetMyDebug(__FUNCTION__ . '()', 'Manual login check..');
        if ($this->GetStatus() !== 102) {
            echo $this->Translate('Module is not configured yet!');
            return false;
        }

        if ($this->Check_Token() === true) {
            $token = $this->ReadAttributeString('AuthToken');
            echo $this->Translate('Successful') . '. AuthToken: ' . $token;
        } else {
            echo $this->Translate('Internet connection, access data and petcare connect account set up correctly?');
        }
    }

    private function Check_Token()
    {
        //Modul is active?
        if ($this->GetStatus() !== 102) {
            echo $this->Translate('Module is not configured yet!');
            return false;
        }
        $endpoint = $this->ReadAttributeString("BaseURL");
        $token = $this->ReadAttributeString('AuthToken');

        //Check Token is OK
        if (!empty($token)) {
            $curl = $this->get_curl($endpoint . "/api/me/start", $token);
            $this->SetMyDebug(__FUNCTION__ . '()', 'Start request..');
            $this->SetMyDebug(__FUNCTION__ . '()', "Request received with code: " . $curl['http_code']);
            if ($curl['http_code'] == "0") {
                $this->SetMyDebug(__FUNCTION__ . '()', '..No internet connection!');
                return false;
            }
            if ($curl['http_code'] == "200") {
                $this->SetMyDebug(__FUNCTION__ . '()', '..token ok');
                $this->SetBuffer("DataArray", serialize($curl));
                return true;
            }
        }
        if (empty($token)) {
            $this->SetMyDebug(__FUNCTION__ . '()', 'Token must be renewed!');
            if ($this->API_Login() == true) {
                $token = $this->ReadAttributeString('AuthToken');
                $curl = $this->get_curl($endpoint . "/api/me/start", $token);
                if ($curl['http_code'] == "200") {
                    $this->SetMyDebug(__FUNCTION__ . '()', '..token ok');
                    $this->SetBuffer("DataArray", serialize($curl));
                    return true;
                }
            }
        }
        $this->SetMyDebug(__FUNCTION__ . '()', 'false');
        return false;
    }

    private function API_Login()
    {
        $email_address = $this->ReadPropertyString("Username");
        $password = $this->ReadPropertyString("Password");
        $device_id = (string) rand(1000000000, 1999999999);
        $endpoint = $this->ReadAttributeString("BaseURL");
        $json = json_encode(array("email_address" => $email_address, "password" => $password, "device_id" => $device_id));
        $curl = $this->post_curl($endpoint . "/api/auth/login", null, $json);
        $this->SetMyDebug(__FUNCTION__ . '()', $endpoint . "/api/auth/login");
        $this->SetMyDebug(__FUNCTION__ . '()', "Re-Request received with code: " . $curl['http_code']);

        if ($curl['http_code'] == "200" and $curl['result']['data']['token']) {
            #print_r($curl);
            $token = $curl['result']['data']['token'];
            $this->WriteAttributeString('AuthToken', $token);
            return true;
        }
        return false;
    }

    private function BatteryState($battery, $Device_Ident, $Device_Name)
    {
        $this->SetMyDebug(__FUNCTION__ . '(Devices)', ' ... Battery: ' . $battery);

        //Create Battery Variable
        $Battery_Ident = preg_replace('![^0-9]!', '', $Device_Ident); //Remove all without numbers
        $Battery_Ident = 'Battery_' . $Battery_Ident;
        if (!@$this->GetIDForIdent($Battery_Ident)) {
            $this->RegisterVariableInteger($Battery_Ident, $Device_Name . ' ' . $this->Translate('battery'), '~Battery.100', 5);
            IPS_SetIcon($this->GetIDForIdent($Battery_Ident), 'Battery');
        }
        $this->Change_IpsVarName($Battery_Ident, $Device_Name . ' ' . $this->Translate('battery'));
        //Volt in percent
        $battery_perc = @$this->BatteryPercent(round($battery, 1));
        if ($battery_perc) {
            $this->SetChangedValue($Battery_Ident, intval($battery_perc));
        }
    }

    private function ConnectionState($Connected, $Device_Ident, $Device_Name)
    {
        $this->SetMyDebug(__FUNCTION__ . '(Devices)', ' ... Connected: ' . $Connected);

        //Create Connection Variable
        $Connection_Ident = preg_replace('![^0-9]!', '', $Device_Ident); //Remove all without numbers
        $Connection_Ident = 'Connection_' . $Connection_Ident;
        if (!@$this->GetIDForIdent($Connection_Ident)) {
            $this->RegisterVariableBoolean($Connection_Ident, $Device_Name . ' ' . $this->Translate('state'), 'SPET.Connection', 5);
        }
        $this->Change_IpsVarName($Connection_Ident, $Device_Name . ' ' . $this->Translate('state'));
        $this->SetChangedValue($Connection_Ident, boolval($Connected));
    }

    private function BatteryPercent($battery)
    {
        if (empty($battery)) {
            return null;
        }
        if ($battery <= 5.05) {
            $battery_perc = 0;
        } elseif ($battery >= 6.0) {
            $battery_perc = 100;
        } else {
            $battery_perc = round((($battery - 5.05) / 1.2) * 100);
        }
        return $battery_perc;
    }

    private function Change_IpsVarName(string $IPS_Ident, string $VL_IpsVariableName) //When changed name in cloud

    {
        if (IPS_GetName($this->GetIDForIdent($IPS_Ident)) != $VL_IpsVariableName) {
            IPS_SetName($this->GetIDForIdent($IPS_Ident), $VL_IpsVariableName);
        }
    }

    private function CheckCredentials()
    {
        $Username = $this->ReadPropertyString('Username');
        $Password = $this->ReadPropertyString('Password');
        if (empty($Username) || empty($Password)) {
            return 201;
        }
        $this->SetMyDebug(__FUNCTION__ . '()', 'ok');
        return 102;
    }

    private function CreateTimelineArray()
    {
        //Create Variable
        if (!@$this->GetIDForIdent('TimelineHTML')) {
            $this->RegisterVariableString('TimelineHTML', $this->Translate('Timeline'), '~HTMLBox', 20);
            IPS_SetIcon($this->GetIDForIdent('TimelineHTML'), 'Database');
        }
        $Array = unserialize($this->GetBuffer("RawTimeline")); //Array from buffer
        //Check empty array
        if (!is_array($Array)) {
            $this->SetMyDebug(__FUNCTION__ . '()', 'No data!');
            return false;
        }
        /*Types
        #0     Haustier geht durch eine Tür
        #1     Batterieschwelle erreicht
        #7     Unbekannte Bewegung der Tür
        #21    Schale aufgefüllt
        #22    Haustier hat gegessen
        #24    Klappe wurde zurückgesetzt
        #29    Haustier hat getrunken
        #30    Felaqua aufgefüllt
        #32    Erinnerung Frischwasser für Felaqua
        #34    Unbekannt hat getrunken
         */
        $TimelineArray = array(); //Reset array
        foreach ($Array as $index => $Line) {
            $type = intval($Line['type']);
            switch ($type) {
                case 0:
                    $TYPE = $Line['type'];
                    $UNIXTIME = strtotime($Line['created_at']);
                    $TIME = date('d.m H:i', strtotime($Line['created_at']));
                    $HOUSE = @$Line['households'][0]['name'];
                    $DEVICE = @$Line['devices'][0]['name'];
                    $PET = @$Line['pets'][0]['name'];
                    $HELPV = @$Line['movements'][0]['direction'];
                    if (empty($DEVICE)) {
                        if ($HELPV === 1) {
                            $TEXT = $this->Translate('Location from ') . $PET . $this->Translate(' changed: ') . $this->Translate('In house');
                        } else {
                            $TEXT = $this->Translate('Location from ') . $PET . $this->Translate(' changed: ') . $this->Translate('Outside');
                        }
                    } else {
                        if ($HELPV === 1) {
                            $TEXT = $PET . ' ' . $this->Translate('has the house through') . ' ' . $DEVICE . ' ' . $this->Translate('entered');
                        } elseif ($HELPV === 2) {
                            $TEXT = $PET . ' ' . $this->Translate('has the house through') . ' ' . $DEVICE . ' ' . $this->Translate('leaving');
                        } elseif ($HELPV === 0) {
                            $TEXT = $PET . ' ' . $this->Translate('has through') . ' ' . $DEVICE . ' ' . $this->Translate('seen');
                        }
                    }
                    break;
                case 1:
                    $TYPE = $Line['type'];
                    $UNIXTIME = strtotime($Line['created_at']);
                    $TIME = date('d.m H:i', strtotime($Line['created_at']));
                    $HOUSE = @$Line['households'][0]['name'];
                    $DEVICE = @$Line['devices'][0]['name'];
                    $PET = '';
                    $HELPV = '';
                    $TEXT = $this->Translate('The battery from ') . $DEVICE . $this->Translate(' is empty!');
                    break;
                case 29:
                    $TYPE = $Line['type'];
                    $UNIXTIME = strtotime($Line['created_at']);
                    $TIME = date('d.m H:i', strtotime($Line['created_at']));
                    $HOUSE = @$Line['households'][0]['name'];
                    $DEVICE = @$Line['devices'][0]['name'];
                    $PET = @$Line['pets'][0]['name'];
                    $HELPV = @$Line['weights'][0]['frames'][0]['change'];
                    $HELPV = round(abs($HELPV));
                    $TEXT = $PET . ' ' . $this->Translate('has') . ' ' . $HELPV . $this->Translate(' ml water from ') . $DEVICE . ' ' . $this->Translate('drunk');
                    break;
                case 30:
                    $TYPE = $Line['type'];
                    $UNIXTIME = strtotime($Line['created_at']);
                    $TIME = date('d.m H:i', strtotime($Line['created_at']));
                    $HOUSE = @$Line['households'][0]['name'];
                    $DEVICE = @$Line['devices'][0]['name'];
                    $PET = '';
                    $HELPV = '';
                    $TEXT = $DEVICE . ' ' . $this->Translate('was refilled');
                    break;
                case 32:
                    $TYPE = $Line['type'];
                    $UNIXTIME = strtotime($Line['created_at']);
                    $TIME = date('d.m H:i', strtotime($Line['created_at']));
                    $HOUSE = @$Line['households'][0]['name'];
                    $DEVICE = @$Line['devices'][0]['name'];
                    $PET = '';
                    $HELPV = '';
                    $TEXT = $this->Translate('Reminder of fresh water for ') . $DEVICE;
                    break;
                case 34:
                    $TYPE = $Line['type'];
                    $UNIXTIME = strtotime($Line['created_at']);
                    $TIME = date('d.m H:i', strtotime($Line['created_at']));
                    $HOUSE = @$Line['households'][0]['name'];
                    $DEVICE = @$Line['devices'][0]['name'];
                    $PET = '';
                    $HELPV = @$Line['weights'][0]['frames'][0]['change'];
                    $HELPV = round(abs($HELPV));
                    $TEXT = $HELPV . $this->Translate(' ml water was taken from ') . $DEVICE . ' ' . $this->Translate('drunk');
                    break;
                default:
                    $TYPE = '';
                    $UNIXTIME = '';
                    $TIME = '';
                    $HOUSE = '';
                    $DEVICE = '';
                    $PET = '';
                    $HELPV = '';
                    $TEXT = '';
                    break;
            }
            //Array befüllen
            $TimelineArray[$index] = compact('TYPE', 'UNIXTIME', 'TIME', 'HOUSE', 'DEVICE', 'PET', 'HELPV', 'TEXT');
        }
        $this->SetMyDebug(__FUNCTION__ . '()', '..ok');
        $this->SetChangeBuffer('Timeline', $TimelineArray);
        $this->CreateTimelineHTML($TimelineArray);
    }

    private function CreateNotificationsHTML($Array)
    {
        //Check empty array
        if (!is_array($Array)) {
            $this->SetMyDebug(__FUNCTION__ . '()', 'No data!');
            return false;
        }
        //Line limit
        $Dataset = $this->ReadPropertyInteger('LineNumbers');
        if ($Dataset > 0) {
            $Array = array_slice($Array, 0, $Dataset, true);
        }
        #print_r($Array);
        // HTML Ausgabe generieren
        $HTML_CSS_Style = $this->StyleHTML();
        $TitelArray = array($this->Translate('Type'), $this->Translate('Time'), $this->Translate('Message'));
        $HTML = $HTML_CSS_Style;
        $HTML .= '<tr><th  class="columA1' . $this->InstanceID . '">' . $TitelArray[0];
        $HTML .= '</th><th class="columA2' . $this->InstanceID . '">' . $TitelArray[1];
        $HTML .= '</th><th class="columA2' . $this->InstanceID . '">' . $TitelArray[2];
        $HTML .= '</th></tr>';

        foreach ($Array as $index => $Zeile) {
            $TIME = date('d.m H:i', strtotime($Zeile['created_at']));
            $HTML .= '<td class="hp-text' . $this->InstanceID . '">' . $Zeile['type'] . '</td>';
            $HTML .= '<td class="hp-text' . $this->InstanceID . '">' . $TIME . '</td>';
            $HTML .= '<td class="hp-text' . $this->InstanceID . '">' . $Zeile['text'] . '</td></tr>';
        }
        $HTML .= '</table></html>';

        //HTML-Tabelle in Variable schreiben
        $this->SetMyDebug(__FUNCTION__ . '()', 'Write hosts table variable');
        $this->SetValue('NotificationsHTML', $HTML);
    }

    private function CreateTimelineHTML($Array)
    {
        //Check empty array
        if (!is_array($Array)) {
            $this->SetMyDebug(__FUNCTION__ . '()', 'No data!');
            return false;
        }
        //Line limit
        $Dataset = $this->ReadPropertyInteger('LineNumbers');
        if ($Dataset > 0) {
            $Array = array_slice($Array, 0, $Dataset, true);
        }
        #print_r($Array);
        // HTML Ausgabe generieren
        $OptionalColumns = $this->ReadPropertyBoolean('OptionalColumns');
        $HTML_CSS_Style = $this->StyleHTML();
        if ($OptionalColumns) {
            $TitelArray = array($this->Translate('Type'), $this->Translate('Time'), $this->Translate('Household'), $this->Translate('Device'), $this->Translate('Pet'), $this->Translate('Message'));
        } else {
            $TitelArray = array($this->Translate('Type'), $this->Translate('Time'), $this->Translate('Message'));
        }
        $HTML = $HTML_CSS_Style;
        $HTML .= '<tr><th  class="columA1' . $this->InstanceID . '">' . $TitelArray[0];
        $HTML .= '</th><th class="columA2' . $this->InstanceID . '">' . $TitelArray[1];
        if ($OptionalColumns) {
            $HTML .= '</th><th class="columA2' . $this->InstanceID . '">' . $TitelArray[2];
            $HTML .= '</th><th class="columA2' . $this->InstanceID . '">' . $TitelArray[3];
            $HTML .= '</th><th class="columA2' . $this->InstanceID . '">' . $TitelArray[4];
            $HTML .= '</th><th class="columA2' . $this->InstanceID . '">' . $TitelArray[5];
        } else {
            $HTML .= '</th><th class="columA2' . $this->InstanceID . '">' . $TitelArray[2];
        }

        $HTML .= '</th></tr>';

        foreach ($Array as $index => $Zeile) {
            if ($Zeile['TYPE'] === '') {
                continue;
            }
            $HTML .= '<td class="hp-text' . $this->InstanceID . '">' . $Zeile['TYPE'] . '</td>';
            $HTML .= '<td class="hp-text' . $this->InstanceID . '">' . $Zeile['TIME'] . '</td>';
            if ($OptionalColumns) {
                $HTML .= '<td class="hp-text' . $this->InstanceID . '">' . $Zeile['HOUSE'] . '</td>';
                $HTML .= '<td class="hp-text' . $this->InstanceID . '">' . $Zeile['DEVICE'] . '</td>';
                $HTML .= '<td class="hp-text' . $this->InstanceID . '">' . $Zeile['PET'] . '</td>';
            }
            $HTML .= '<td class="hp-text' . $this->InstanceID . '">' . $Zeile['TEXT'] . '</td></tr>';
        }
        $HTML .= '</table></html>';

        //HTML-Tabelle in Variable schreiben
        $this->SetMyDebug(__FUNCTION__ . '()', 'Write TimelineHTML variable');
        $this->SetValue('TimelineHTML', $HTML);
    }

    private function StyleHTML()
    {
        /* Code by Bayaro
        HTML CSS Style definieren (Tabelle, Schrift, Farben, ...) */
        $HTMLFontColor = substr('000000' . dechex($this->ReadPropertyInteger('HTMLFontColor')), -6);
        $HTMLBG = substr('000000' . dechex($this->ReadPropertyInteger('HTMLBG')), -6);
        //Transparent?
        if ($this->ReadPropertyInteger('HTMLBG') < 0) {
            $HTMLBG = 'fade(#FFFFFF, 50%)';
        }
        //Schriftart
        $Fontnumber = $this->ReadPropertyInteger('HTMLFontType');
        switch ($Fontnumber) {
            case 0:
                $FontString = 'font-family: inherit;';
                break;
            case 1:
                $FontString = 'font-family: "Consolas";';
                break;
            case 2:
                $FontString = 'font-family: "Courier";';
                break;
            case 3:
                $FontString = 'font-family: "DejaVu Sans Mono";';
                break;
        }
        $FontString0 = $FontString . 'font-size:' . $this->ReadPropertyInteger('HTMLFontSize') . 'px; color:#' . $HTMLFontColor . '; text-align:left;';

        $Css1 = '<html lang="en">';
        $Css1 = $Css1 . '<style type="text/css">';
        $Css1 = $Css1 . '.hp {width:100%; white-space:normal; border-collapse:collapse; border: 1px solid rgba(255, 255, 255, 0.1);}';
        $Css1 = $Css1 . '.hp td' . $this->InstanceID . ' {font-size:' . $this->ReadPropertyInteger('HTMLFontSize') . 'px; color:#000000; overflow:hidden; word-break:normal;}';
        $Css1 = $Css1 . '.hp th' . $this->InstanceID . ' {font-size:' . $this->ReadPropertyInteger('HTMLFontSize') . 'px; color:#FFFFFF; overflow:hidden; word-break:normal; }';
        $Css1 = $Css1 . '.hp .hp-text' . $this->InstanceID . '{' . $FontString0 . ' background-color:#' . $HTMLBG . '; flex: 1; padding:' . $this->ReadPropertyInteger('HTMLPaddingHigh') . 'px; padding-right: ' . $this->ReadPropertyInteger('HTMLPadding2') . 'px; border-collapse:collapse; border: 1px solid rgba(255, 255, 255, 0.1); }';
        $Css1 = $Css1 . '.hp .columA1' . $this->InstanceID . '{' . $FontString0 . ' width:' . $this->ReadPropertyInteger('HTMLPadding1') . 'px; padding:' . $this->ReadPropertyInteger('HTMLPaddingHigh') . 'px;}';
        $Css1 = $Css1 . '.hp .columA2' . $this->InstanceID . '{' . $FontString0 . ' width:' . $this->ReadPropertyInteger('HTMLPadding2') . 'px; padding:' . $this->ReadPropertyInteger('HTMLPaddingHigh') . 'px;}';
        $Css1 = $Css1 . '</style>';

        $Css2 = '<style media="screen" type="text/css">';
        $Css2 = $Css2 . '    @media only screen and (max-device-width: 480px) {';
        $Css2 = $Css2 . '.hp td' . $this->InstanceID . ' {font-size:8px;}';
        $Css2 = $Css2 . '.hp th' . $this->InstanceID . ' {font-size:8px;}';
        $Css2 = $Css2 . '.hp .hp-text' . $this->InstanceID . ' {font-size:8px;}';
        $Css2 = $Css2 . '.hp .columA1' . $this->InstanceID . ' {font-size:8px;}';
        $Css2 = $Css2 . '.hp .columA2' . $this->InstanceID . ' {font-size:8px;}';
        $Css2 = $Css2 . '}';
        $Css2 = $Css2 . '</style>';

        $Css3 = '<table class="hp">';

        if ($this->ReadPropertyBoolean('MobileFont')) {
            return $Css1 . $Css2 . $Css3;
        }
        return $Css1 . $Css3;
    }

    public function CleanToken()
    {
        $this->SetMyDebug(__FUNCTION__ . '()', 'token was deleted!');
        //Delete attribute
        $this->WriteAttributeString('AuthToken', '');
        $this->WriteAttributeInteger('HouseholdID', 0);
    }

    public function CleanUp()
    {
        $this->SetMyDebug(__FUNCTION__ . '()', 'all instance variables was deleted!');
        //Delete module variables
        foreach (IPS_GetChildrenIDs($this->InstanceID) as $ObjectID) {
            IPS_DeleteVariable($ObjectID);
        }
        //Delete module profiles
        @IPS_DeleteVariableProfile('SPET.LED');
        @IPS_DeleteVariableProfile('SPET.Lockmode');
        @IPS_DeleteVariableProfile('SPET.PetLocation');
        @IPS_DeleteVariableProfile('SPET.PetDrinking');
        //Delete buffer
        @$this->SetBuffer('DataArray', '');
        @$this->SetBuffer('Notifications', '');
        @$this->SetBuffer('RawTimeline', '');
        @$this->SetBuffer('Timeline', '');
        //Create profiles
        $this->CreateProfiles();
    }

    private function CreateProfiles()
    {
        if (!IPS_VariableProfileExists('SPET.Connection')) {
            IPS_CreateVariableProfile('SPET.Connection', 0);
            IPS_SetVariableProfileIcon('SPET.Connection', 'Cloud');
            IPS_SetVariableProfileAssociation('SPET.Connection', 1, $this->Translate('Connected'), '', -1);
            IPS_SetVariableProfileAssociation('SPET.Connection', 0, $this->Translate('Disconnected'), '', 0xFF0000);
        }
        if (!IPS_VariableProfileExists('SPET.Level')) {
            IPS_CreateVariableProfile('SPET.Level', 1);
            IPS_SetVariableProfileIcon('SPET.Level', 'Drops');
            IPS_SetVariableProfileDigits("SPET.Level", 0);
            IPS_SetVariableProfileText("SPET.Level", "", " ml");
        }
        if (!IPS_VariableProfileExists('SPET.LED')) {
            IPS_CreateVariableProfile('SPET.LED', 1);
            IPS_SetVariableProfileIcon('SPET.LED', 'Intensity');
            IPS_SetVariableProfileAssociation('SPET.LED', 0, $this->Translate('Off'), '', -1);
            IPS_SetVariableProfileAssociation('SPET.LED', 1, $this->Translate('Dark'), '', -1);
            IPS_SetVariableProfileAssociation('SPET.LED', 2, $this->Translate('Bright'), '', -1);
        }
        if (!IPS_VariableProfileExists('SPET.Lockmode')) {
            IPS_CreateVariableProfile('SPET.Lockmode', 1);
            IPS_SetVariableProfileIcon('SPET.Lockmode', 'LockOpen');
            IPS_SetVariableProfileAssociation('SPET.Lockmode', 0, $this->Translate('None'), 'LockOpen', -1);
            IPS_SetVariableProfileAssociation('SPET.Lockmode', 1, $this->Translate('Lock in'), 'LockClosed', 0x00FF00);
            IPS_SetVariableProfileAssociation('SPET.Lockmode', 2, $this->Translate('Lock out'), 'LockClosed', 0xFF0000);
            IPS_SetVariableProfileAssociation('SPET.Lockmode', 3, $this->Translate('Lock both'), 'Lock', 0xFF0000);
        }
        if (!IPS_VariableProfileExists('SPET.PetLocation')) {
            IPS_CreateVariableProfile('SPET.PetLocation', 1);
            IPS_SetVariableProfileIcon('SPET.PetLocation', 'Cat');
            #IPS_SetVariableProfileIcon('SPET.PetLocation', 'Dog');
            IPS_SetVariableProfileAssociation('SPET.PetLocation', 1, $this->Translate('In house'), '', 0x00FF00);
            IPS_SetVariableProfileAssociation('SPET.PetLocation', 2, $this->Translate('Outside'), '', 0xFF0000);
        }
        return;
    }

    private function FindDeviceNamebyID($petlastmotiondevice)
    {
        //Get data from buffer
        $data_array = unserialize($this->GetBuffer("DataArray"));
        $devices = @$data_array['result']['data']['devices'];
        if ($devices) {
            $index = array_search($petlastmotiondevice, array_column($devices, 'id'));
            $devicename = @$devices[$index]['name'];
            if ($devicename) {
                return $devicename;
            } else {
                return '';
            }
        }
    }

    private function GetOptionalsData()
    {
        $endpoint = $this->ReadAttributeString("BaseURL");
        $token = $this->ReadAttributeString('AuthToken');

        $Notifications = $this->ReadPropertyBoolean('Notifications');
        $Timeline = $this->ReadPropertyBoolean('Timeline');

        /*Endpoints
        $curl = $this->get_curl($endpoint . "/api/start", $token);          //start
        $curl = $this->get_curl($endpoint . "/api/notification", $token);   //notification
        $curl = $this->get_curl($endpoint . "/api/timeline", $token);       //timeline
        $curl = $this->get_curl($endpoint . "/api/household", $token);      //household
        $curl = $this->get_curl($endpoint . "/api/product", $token);        //product
        $curl = $this->get_curl($endpoint . "/api/tag", $token);            //tag
        $curl = $this->get_curl($endpoint . "/api/pet", $token);            //pet
        */

        $Household_ID = $this->ReadAttributeInteger('HouseholdID');

        //Timeline
        $curl = $this->get_curl($endpoint . "/api/timeline/household/" . $Household_ID, $token);
        #print_r($curl);
        $ArrayTimeline = @$curl['result']['data'];
        if ($curl['http_code'] == "200") {
            $this->SetMyDebug(__FUNCTION__ . '()', '..Timeline ok');
            //HTML
            if ($Timeline === true) {
                $Result = $this->SetChangeBuffer('RawTimeline', $ArrayTimeline);
                if ($Result or !@$this->GetIDForIdent('TimelineHTML')) {
                    $this->CreateTimelineArray();
                }
            } else {
                @IPS_DeleteVariable(IPS_GetObjectIDByIdent('TimelineHTML', $this->InstanceID));
            }
        } else {
            $this->SetMyDebug(__FUNCTION__ . '()', '..Timeline error');
        }

        //Felaqua Variables
        if (@$ArrayTimeline[0]['devices'][0]['product_id'] === 8) {
            $this->SetMyDebug(__FUNCTION__ . '()', '..Felaqua readings');
            $FelaquaDate_Ident = 'FelaquaDate_' . @$ArrayTimeline[0]['devices'][0]['id'];
            $FelaquaLevel_Ident = 'FelaquaLevel_' . @$ArrayTimeline[0]['devices'][0]['id'];
            $FelaquaName = $ArrayTimeline[0]['devices'][0]['name'];
            $FelaquaDate = $ArrayTimeline[0]['devices'][0]['updated_at'];
            $FelaquaDate2 = strtotime($FelaquaDate); //from UTC to Local Time
            $FelaquaLevel = @$ArrayTimeline[0]['weights'][0]['frames'][0]['current_weight'];
            //Fill date
            if (!@$this->GetIDForIdent($FelaquaDate_Ident)) {
                $this->RegisterVariableInteger($FelaquaDate_Ident, $FelaquaName . ' ' . $this->Translate('last refilled on'), '~UnixTimestamp', 5);
                IPS_SetIcon($this->GetIDForIdent($FelaquaDate_Ident), 'Tap');
            }
            $this->SetChangedValue($FelaquaDate_Ident, $FelaquaDate2);
            //Level
            if (!@$this->GetIDForIdent($FelaquaLevel_Ident)) {
                $this->RegisterVariableInteger($FelaquaLevel_Ident, $FelaquaName . ' ' . $this->Translate('Level'), 'SPET.Level', 5);
            }
            $this->SetChangedValue($FelaquaLevel_Ident, intval(round(abs($FelaquaLevel)), 0));
        }

        //Notifications
        if ($Notifications === true) {
            $curl = $this->get_curl($endpoint . "/api/notification", $token);
            #print_r($curl);
            $ArrayNotifications = @$curl['result']['data'];
            if ($curl['http_code'] == "200") {
                $this->SetMyDebug(__FUNCTION__ . '()', '..Notifications ok');
                #$this->SetBuffer("Notifications", serialize($curl));
                $Result = $this->SetChangeBuffer('Notifications', $ArrayNotifications);
                if ($Result) {
                    //Create Variable
                    if (!@$this->GetIDForIdent('NotificationsHTML')) {
                        $this->RegisterVariableString('NotificationsHTML', $this->Translate('Notifications'), '~HTMLBox', 20);
                        IPS_SetIcon($this->GetIDForIdent('NotificationsHTML'), 'Database');
                    }
                    $this->CreateNotificationsHTML($ArrayNotifications);
                }
            } else {
                $this->SetMyDebug(__FUNCTION__ . '()', '..Notifications error');
            }
        } else {
            @IPS_DeleteVariable(IPS_GetObjectIDByIdent('NotificationsHTML', $this->InstanceID));
        }
    }

    private function SetChangeBuffer($Buffername, $ArrayName)
    {
        $OldBuffer = unserialize($this->GetBuffer($Buffername));
        if ($OldBuffer !== $ArrayName) {
            $this->SetBuffer($Buffername, serialize($ArrayName));
            return true;
        }
        return false;
    }

    private function PetDetection($pet, $Pet_LocationText)
    {
        $PetDetectionVariable = $this->ReadPropertyBoolean('PetDetectionVariable');
        $Petmotion_Ident = 'PetMotion_' . $pet['id'];
        //Get PetLocationTime
        $Pet_LocationTime = @$pet['position']['since'];
        $Pet_LocalTime = strtotime($Pet_LocationTime); //from UTC to Local Time
        $Pet_LocalSince = date('d.m H:i', $Pet_LocalTime); //Short format day/hour
        $Pet_LocalSinceDay = date('d.m', $Pet_LocalTime); //Short format day
        $Pet_LocalSinceTime = date('H:i', $Pet_LocalTime); //Short format hour

        //Get PetLastMotion
        $Pet_LastmotionDeviceID = @$pet['position']['device_id'];
        if (!empty($Pet_LastmotionDeviceID)) {
            $Pet_MotionDevice = $this->FindDeviceNamebyID($Pet_LastmotionDeviceID);
            $this->SetMyDebug(__FUNCTION__ . '(Pets)', ' ... Detection: ' . $Pet_LocalSince);
            if ($Pet_MotionDevice) {
                #CreateVariable for PetMotion
                if (!@$this->GetIDForIdent($Petmotion_Ident)) {
                    $this->RegisterVariableString($Petmotion_Ident, $pet['name'] . '\'s ' . $this->Translate('last motion'), '', 11);
                    IPS_SetIcon($this->GetIDForIdent($Petmotion_Ident), 'Repeat');
                }
                $VL_IpsVariableName = $Pet_MotionDevice . ' ' . $this->Translate('at') . ' ' . $Pet_LocalSinceDay . ' ' . $this->Translate('at ') . ' ' . $Pet_LocalSinceTime . 'h ' . $this->Translate($Pet_LocationText);
                $this->SetChangedValue($Petmotion_Ident, $VL_IpsVariableName);
            }
        }
    }

    public function RequestAction($Ident, $Value)
    {
        $endpoint = $this->ReadAttributeString("BaseURL");
        $token = $this->ReadAttributeString('AuthToken');
        #echo $Ident;
        switch ($Ident) {
            case (bool) preg_match('/^Hub_[0-9]{5,6}/', $Ident):
                $Hub_Ident = str_replace('Hub_', '', $Ident);
                $this->SetValue($Ident, $Value);
                $this->SetMyDebug(__FUNCTION__ . '(led_mode)', $Value);
                if ($Value == 1) {
                    $Value = 4;
                }
                if ($Value == 2) {
                    $Value = 1;
                }
                $json = json_encode(array("led_mode" => $Value));
                $curl = $this->put_curl($endpoint . "/api/device/$Hub_Ident/control", $token, $json);
                break;
            case (bool) preg_match('/^Flap_[0-9]{5,6}/', $Ident):
                $Flap_Ident = str_replace('Flap_', '', $Ident);
                $this->SetValue($Ident, $Value);
                $this->SetMyDebug(__FUNCTION__ . '(locking)', $Value);
                $json = json_encode(array("locking" => $Value));
                $curl = $this->put_curl($endpoint . "/api/device/$Flap_Ident/control", $token, $json);
                break;
            case (bool) preg_match('/^PetLocation_[0-9]{5,6}/', $Ident):
                #PetID wird aus der Variablen Eigenschaft IDENT ausgelesen!
                $Pet_Ident = str_replace('PetLocation_', '', $Ident);
                $this->SetValue($Ident, $Value);
                $this->SetMyDebug(__FUNCTION__ . '(pet_location)', $Value);
                $json = json_encode(array("where" => $Value, "since" => gmdate("Y-m-d H:i"))); //gmdate for utc-time
                $curl = $this->post_curl($endpoint . "/api/pet/$Pet_Ident/position", $token, $json);
                break;
            default:
                throw new Exception("Invalid ident");
        }
    }

    public function ReadArray()
    {
        $InfoArray = unserialize($this->GetBuffer("DataArray"));
        if (!empty($InfoArray)) {
            return $InfoArray;
        } else {
            return $this->Translate('No data! Perform an update first');
        }
    }

    public function ReadHousehold()
    {
        $InfoArray = unserialize($this->GetBuffer("DataArray"));
        if (!empty($InfoArray)) {
            return $InfoArray['result']['data']['households'];
        } else {
            return $this->Translate('No data! Perform an update first');
        }
    }

    public function ReadDevices()
    {
        $InfoArray = unserialize($this->GetBuffer("DataArray"));
        if (!empty($InfoArray)) {
            return $InfoArray['result']['data']['devices'];
        } else {
            return $this->Translate('No data! Perform an update first');
        }
    }

    public function ReadPets()
    {
        $InfoArray = unserialize($this->GetBuffer("DataArray"));
        if (!empty($InfoArray)) {
            return $InfoArray['result']['data']['pets'];
        } else {
            return $this->Translate('No data! Perform an update first');
        }
    }

    public function ReadNotifications()
    {
        if ($this->ReadPropertyBoolean('Notifications')) {
            $InfoArray = unserialize($this->GetBuffer("Notifications"));
            if (!empty($InfoArray)) {
                return $InfoArray;
            } else {
                return $this->Translate('No data! Perform an update first');
            }
        } else {
            return $this->Translate('Optional data are disabled!');
        }
    }

    public function ReadTimeline()
    {
        if ($this->ReadPropertyBoolean('Timeline')) {
            $InfoArray = unserialize($this->GetBuffer("Timeline"));
            if (!empty($InfoArray)) {
                return $InfoArray;
            } else {
                return $this->Translate('No data! Perform an update first');
            }
        } else {
            return $this->Translate('Optional data are disabled!');
        }
    }

    public function MessageSink($TimeStamp, $SenderID, $Message, $Data)
    {
        if ($Message === IPS_KERNELSTARTED) {
            $this->ApplyChanges();
        }
        return true;
    }

    private function ModulInfo()
    {
        return IPS_GetLibrary('{FA56C456-3167-7242-7CD3-4969ED5A3F3F}');
    }

    private function SetChangedValue($Value1, $Value2)
    {
        if ($this->GetValue($Value1) !== $Value2) {
            SetValue($this->GetIDForIdent($Value1), $Value2);
        }
    }

    private function SetMyDebug($Message, $DATA)
    {
        if ($this->ReadPropertyBoolean('Debug')) {
            $this->SendDebug($Message, $DATA, 0);
        }
    }

    private function SetMyTimerInterval()
    {
        if ($this->ReadPropertyBoolean('UpdateTimer') === true) {
            $Interval = $this->ReadPropertyInteger('UpdateIntervall') * 1000;
            $this->SetTimerInterval('SPET_Update', $Interval);
        } else {
            $this->SetTimerInterval('SPET_Update', 0);
        }
    }

}
