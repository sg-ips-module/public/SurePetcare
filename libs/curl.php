<?php

trait SPET_CURL
{

    protected function init_curl($endpoint) {
		$ch = curl_init((string)$endpoint);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Linux; Android 7.0; SM-G930F Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/64.0.3282.137 Mobile Safari/537.36");
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4); //Zeit für Verbindungsaufbau
		curl_setopt($ch, CURLOPT_TIMEOUT, 6); //Zeit für Befehlsverarbeitung
		return $ch;
	}

    protected function start_curl($ch) {
		$curl_array = array("result" => "", "http_code" => 0);	
		$curl_array['result'] = json_decode(curl_exec($ch),true);
		$curl_array['http_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		return $curl_array;
	}

    protected function get_curl($endpoint, $token) {
		$ch = $this->init_curl((string)$endpoint);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer $token"));
		return $this->start_curl($ch);
	}

    protected function put_curl($endpoint, $token, $json) {
		$ch = $this->init_curl((string)$endpoint);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Content-Length: ".strlen($json),"Authorization: Bearer $token"));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);	
		return $this->start_curl($ch);
	}

    protected function post_curl($endpoint, $token, $json) {
		$ch = $this->init_curl($endpoint);
		if(empty($token)) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Content-Length: ".strlen($json)));
		} else {
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Content-Length: ".strlen($json),"Authorization: Bearer $token"));
		}
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		return $this->start_curl($ch);
	}

}
